package extreme.tddscaffold;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tddscaffold {
    public static final String General_Merchandise = "General_Merchandise";
    public static final String Backstage_Pass = "Backstage_Pass";
    public static final String Sulfura = "Sulfura";
    public static final String Aged_Bri = "Aged_Bri";

    public String say(String type, int sellIn, int quality) {
        String result = toSellIn(type, sellIn);
        if (type.equals(General_Merchandise)) {
            if (sellIn > 0) {
                if (quality != 0) {
                    quality -= 1;
                }
            } else {
                quality -= 2;
            }
        }
        if (type.equals(Backstage_Pass)) {
            if (sellIn > 10) {
                quality += 1;
            }
            if (sellIn <= 10 && sellIn > 5) {
                quality += 2;
            }
            if (sellIn <= 5) {
                quality += 3;
            }
            if (sellIn == 0) {
                quality = 0;
            }
            if (quality > 50) {
                quality = 50;
            }
        }

        if (type.equals(Aged_Bri)) {
            if (sellIn > 0) {
                quality += 1;
            }
            if (sellIn <= 0) {
                quality += 2;
            }
            if (quality > 50) {
                quality = 50;
            }
        }
        result += String.valueOf(quality);
        return result;
    }

    public String toSellIn(String type, int sellIn) {
        String result = "";
        if (type.equals(Sulfura)) {
            return result += String.valueOf(sellIn) + ",";
        } else {
            return result += String.valueOf(sellIn - 1) + ",";
        }
    }
}
